<!DOCTYPE HTML>
<head>
<meta content="text/html;charset=utf-8" http-equiv="Content-Type">
<title>Calculator</title>
</head>
<body>
<?php

if(empty($_GET["num1"]) || empty($_GET["num2"])){
	echo "Missing fields";
}
else{
	if(!empty($_GET["math"])){
		$num1 = $_GET["num1"];
		$num2 = $_GET["num2"];
		$math = $_GET["math"];
		$ans = 0;
		if($math == "add"){
			$ans = $num1 + $num2;
		}
		else if($math == "subtract"){
			$ans = $num1 - $num2;
		}
		else if($math == "multiply"){
			$ans = $num1 * $num2;
		}
		else{
			if($num2 != 0){
				$ans = $num1 / $num2;	
			}
		}
		if ($num2 == 0 && $math == "divide"){
			echo "Cannot divide by 0";
		}
		else{
			echo "The answer is $ans.";	
		}
	}
}
?>
</body>
